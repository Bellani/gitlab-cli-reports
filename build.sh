#!/bin/sh

# Abort on error.
set -e

if [ -z "$1" ]; then
  echo 'Building dev binary...'
else
  echo "Building prod binary (version '$1')..."

  # TODO: Ensure there are no uncommited changes to this file first.
  echo "__version__ = '$1'" > _version.py
fi

pipenv run pyinstaller \
  --additional-hooks-dir pyinstaller-hooks \
  --clean --onefile \
  --name gitlab-cli-reports \
  main.py

if [ -n "$1" ]; then
  git checkout -- _version.py
fi

echo "  Success! The binary is located at $(pwd)/dist/gitlab-cli-reports"
