# gitlab-cli-reports


## About

gitlab-cli-reports is a command line tool for generating timesheet reports from GitLab data.


## Usage

See usage information via the following command:

```sh
gitlab-cli-reports --help
```

Example usage:

```sh
gitlab-cli-reports \
  --host-url https://gitlab.example.com \
  --access-token 123isnotarealtoken \
  --filter-by-date-begin 2000-01-01 \
  --filter-by-author john.doe
```


## Developing

### Setup

Ensure you have the following dependencies installed.

* python-3.5
* pipenv

Then run the following:

```sh
pipenv install
```


### Running

Use `pipenv` to run the script, like so:

```sh
pipenv run python3 main.py --help
```


### Building

From the root of the repo, run the following:

```sh
./build.sh
```

The resulting binary can then be found at `./dist/gitlab-cli-reports`.
